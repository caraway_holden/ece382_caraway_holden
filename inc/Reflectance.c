// Reflectance.c

//Documentation: Maj Birrer assisted by helping me execute my
//ideas in Reflectance_Center() as well as error check my other
//Reflectance.c functions. After about 1.5 hours, we found that
//my code was correct and that I most likely had damaged sensors or
//messed up board. After running my code on other sensors and robots,
//I have verified this idea, since my code ran correctly on
//other robots. I believe it may be in part to my sensors AND my board
//based on the results from checking each solution individually.
//C2C Song assisted on the Reflectance_Position() by clarifying that we
//are testing whether any of the sensors see the line or if the
//robot is completely off the line with no sensors seeing the line. I had
//been under the impression that still at least one part of the sensors
//were still seeing the line. This allowed me to craft the first
//if statement checking if data is == 0.


// Provide functions to take measurements using the kit's built-in
// QTRX reflectance sensor array.  Pololu part number 3672. This works by outputting to the
// sensor, waiting, then reading the digital value of each of the
// eight phototransistors.  The more reflective the target surface is,
// the faster the voltage decays.
// Daniel and Jonathan Valvano
// July 11, 2019

/* This example accompanies the book
   "Embedded Systems: Introduction to Robotics,
   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2019, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

// reflectance even LED illuminate connected to P5.3
// reflectance odd LED illuminate connected to P9.2
// reflectance sensor 1 connected to P7.0 (robot's right, robot off road to left)
// reflectance sensor 2 connected to P7.1
// reflectance sensor 3 connected to P7.2
// reflectance sensor 4 connected to P7.3 center
// reflectance sensor 5 connected to P7.4 center
// reflectance sensor 6 connected to P7.5
// reflectance sensor 7 connected to P7.6
// reflectance sensor 8 connected to P7.7 (robot's left, robot off road to right)

#include <stdint.h>
#include "msp432.h"
#include "..\inc\Clock.h"

volatile uint8_t sensorReading;


// ------------Reflectance_Init------------
// Initialize the GPIO pins associated with the QTR-8RC
// reflectance sensor.  Infrared illumination LEDs are
// initially off.
// Initialize even LED Illuminate connected to P5.3
// Initialize odd LED Illuminate connected to P9.2
// Initialize reflectance sensors connected to P7.0-P7.7
// Input: none
// Output: none
void Reflectance_Init(void){

    P5->SEL0 &= ~(0x08);    //initializes P5 as output
    P5->SEL1 &= ~(0x08);
    P5->DIR |= (0x08);
    P5->REN |= (0x08);  //enables as pull up resistor
    P5->OUT &= ~(0x08);

    P9->SEL0 &= ~(0x04);    //initializes P9 as output
    P9->SEL1 &= ~(0x04);
    P9->DIR |= (0x04);
    P9->REN |= (0x04);  //enables as pull up resistor
    P9->OUT &= ~(0x04);

    P5->OUT &= ~(0x08);     //turns P5 off

    P9->OUT &= ~(0x04);     //turns P9 off

    P7->SEL0 &= ~(0xFF);    //initializes P7 as input
    P7->SEL1 &= ~(0xFF);
    P7->DIR &= ~(0xFF);

    P7->REN &= ~(0xFF);     //does not enable P7 as pull resistor

}

// ------------Reflectance_Read------------
// Read the eight sensors
// Turn on Even and Odd IR LEDs
// Pulse the 8 sensors high for 10 us
// Make the sensor pins input
// wait time us
// Read sensors
// Turn off Even and Odd IR LEDs
// Input: time to wait in usec
// Output: sensor readings
// Assumes: Reflectance_Init() has been called
uint8_t Reflectance_Read(uint32_t time){

    uint8_t result;         //initializes variable result
//    uint8_t data;           //initializes variable data

    P5->OUT |= (0x08);      //turns on even LEDs
    P9->OUT |= (0x04);      //turns on odd LEDs

    P7->DIR = (0xFF);      //sets P7 as output
    P7->OUT |= (0xFF);     //sets P7 output to high (on)

    Clock_Delay1us(10);      //waits 10 microseconds to charge the capacitor of the sensors; rate of discharge depends on 0 or 1 (b or w)

//    P7->SEL0 &= ~(0xFF);    //clears selected bits of P7
//    P7->SEL1 &= ~(0xFF);    //clears selected bits of P7
//    SEL is used to initialize as i/o

    P7->DIR &= ~(0xFF);      //sets P7 as input

    Clock_Delay1us(time);       //wait for "time"

    result = (P7->IN & (0xFF));   //reads the data in P7

    P5->OUT &= ~(0x08);         //turns off the even LEDs
    P9->OUT &= ~(0x04);         //turns off the odd LEDs

  return result;        //returns the result
}

// ------------Reflectance_Center------------
// Read the two center sensors
// Turn on the 8 IR LEDs
// Pulse the 8 sensors high for 10 us
// Make the sensor pins input
// wait t us
// Read sensors
// Turn off the 8 IR LEDs
// Input: time to wait in usec
// Output: 0 (off road), 1 off to left, 2 off to right, 3 on road
// (Left,Right) Sensors
// 1,1          both sensors   on line
// 0,1          just right     off to left
// 1,0          left left      off to right
// 0,0          neither        lost
// Assumes: Reflectance_Init() has been called
uint8_t Reflectance_Center(uint32_t time){

    uint8_t centerSensors =  Reflectance_Read(time);  //sets centerSensors equal to result

    centerSensors = (centerSensors & 0x18) >> 3;

//    centerSensors >> 3;

  return centerSensors; // replace this line
}

const int32_t Weight[8] = {334, 238, 142, 48, -48, -142, -238, -334};       // 0.1 mm
const int32_t Mask[8] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};


// Perform sensor integration
// Input: data is 8-bit result from line sensor
// Output: position in 0.1mm relative to center of line
int32_t Reflectance_Position(uint8_t data){


    static int8_t sign = 0;
    int i = 0;              //initializes i, used to iterate through the sensor info
    int sum = 0;            //initializes sum, all weights added to this variable
    int count = 0;          //initializes count, keeps count of how many iterations/data we go through
//    int average = 0;        //initializes average, will be the sum divided by count

    if(data) {
        for(i = 0; i < 8; i++) {
            if(data & Mask[i]) {
                sum += Weight[i];
                count++;
            }
        }
        sign = (sum > 0) ? 1:-1;
        return (sum/count);
    }
    else {
        return (sign * Weight[0]);
    }

//    int i = 0;              //initializes i, used to iterate through the sensor info
//    int sum = 0;            //initializes sum, all weights added to this variable
//    int count = 0;          //initializes count, keeps count of how many iterations/data we go through
//    int average = 0;        //initializes average, will be the sum divided by count
//
//    //if robot is not on line, return set value; otherwise go into for loop
//    if (data == 0) {
//        return data;
//    }
//
//    //else, if robot is on line then iterate through sensor information
//    else {
//
//    //this iterates through the sensor data
//    for (i=0; i<8; i++) {
//        if((Mask[i] & data) != 0) {        //checks if AND result of Mask[i] and data is not zero
//          sum += Weight[i];                 //adds current weight to the sum
//          count += 1;                       //increments count
//        }
//        average = sum / count;              //gets average by dividing total by number of sensors counted
//        }
//    }
//    return average;     // returns result
}

// ------------Reflectance_Start------------
// Begin the process of reading the eight sensors
// Turn on the 8 IR LEDs
// Pulse the 8 sensors high for 10 us
// Make the sensor pins input
// Input: none
// Output: none
// Assumes: Reflectance_Init() has been called
void Reflectance_Start(void){
//    P5->DIR |= (0x08);      //sets to output
    P5->OUT |= (0x08);        //sets P5 to high

//    P9->DIR |= (0x04);      //sets to output
    P9->OUT |= (0x04);        //sets P9 to high

    P7->DIR = (0xff);      //sets to output
    P7->OUT = (0xff);            //sets P7 to high

    Clock_Delay1us(10);

//    P7->OUT &= ~(0xff);            //turns off P7

    P7->REN &= ~(0xff);
    P7->DIR &= ~(0xff);        //sets P7 to input

}


// ------------Reflectance_End------------
// Finish reading the eight sensors
// Read sensors
// Turn off the 8 IR LEDs
// Input: none
// Output: sensor readings
// Assumes: Reflectance_Init() has been called
// Assumes: Reflectance_Start() was called 1 ms ago
uint8_t Reflectance_End(void){
    // write this as part of Lab 10


    sensorReading = ~(P7->IN);

//    P5->DIR |= (0x08);
    P5->OUT &= ~(0x08);        //sets P5 to high

//    P9->DIR |= (0x04);
    P9->OUT &= ~(0x04);        //sets P9 to high

 return sensorReading; // returns the data from sensors
}

