; LEDs.asm
; C2C Holden Caraway
; 28 Sep 2020
; Documentation: Included in Lab08_LED_Swithcesmain.c
; Runs on MSP432
; Capt Steven Beyer
; September 9, 2019


;	Code to activate LED on P6.7. This code accompanies the Lab08_LED_Switchesmain.c
;
       .thumb
       .text
       .align 2
       .global LED_Init
       .global LED_Off
       .global LED_On
       .global LED_Toggle
       .global LED_Oscillate

; function to initialize P6.7
LED_Init:	.asmfunc
	LDR R1, P6SEL0
	LDRB R0, [R1]
	BIC R0, R0, #0x80	; GPIO
	STRB R0, [R1]
	LDR R1, P6SEL1
	LDRB R0, [R1]
	BIC R0, R0, #0x80
	STRB R0, [R1]		; GPIO
	LDR R1, P6DIR
	LDRB R0, [R1]
	ORR R0, R0, #0x80	; output
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to turn off P6.7
LED_Off:		.asmfunc
	LDR R1, P6OUT
	LDRB R0, [R1]		; 8-bit read
	BIC R0, R0, #0x80	; turn off
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to turn on P6.7
LED_On:	.asmfunc
	LDR R1, P6OUT
	LDRB R0, [R1]		; 8-bit read
	ORR R0, R0, #0x80	; turn on
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to toggle P6.7
LED_Toggle: .asmfunc
	LDR R1, P6OUT		;loads bits of P6OUT into R1
	LDRB R0, [R1]		;8-bit read
	EOR R0, R0, #0x80	;toggle
	STRB R0, [R1]		;stores bits
	BX LR				;breaks
	.endasmfunc

; function to continuously toggle P6.7 every half second
; use a loop as a timer
LED_Oscillate: .asmfunc
;	LDR R1, P6OUT		;loads P6OUT bits into R1
;	LDRB R0, [R1]		;8-bit read

loop2
	LDR R3, DELAY

loop1
	SUB R3, R3, #1
	CMP R3, #0
	BNE loop1
	BL LED_Toggle
	B loop2

	.endasmfunc

; addresses for Port 6 registers
	.align 4
P6SEL0 .word 0x40004C4B
P6SEL1 .word 0x40004C4D
P6DIR  .word 0x40004C45
P6OUT  .word 0x40004C43
DELAY  .word 2000000
	.end
