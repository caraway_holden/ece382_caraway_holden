// Lab17_Control.c
// Runs on MSP432
// Implementation of the control system.
// Daniel and Jonathan Valvano
// July 11, 2019

/* This example accompanies the book
   "Embedded Systems: Introduction to Robotics,
   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

 C2C Holden Caraway
 Date: 13 Nov 2020
 Documentation: C2C Couch assisted by explaining the function of Kp/Ka/Kb in regard to the adjustment speed/magnitude as well as
     error adjustment for the motors.vC2C Olszewski assisted by helping me find that I had changed the wrong UDec to SDec per Dr Baek's instructions
     to change lines 138 and 140 because I had typed above those lines and moved them down. Maj Birrer



Simplified BSD License (FreeBSD License)
Copyright (c) 2019, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/


#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"


//struct State{
//    char name[7];                       // name of state
//    void (*fnctPt)(void);           // function to execute during state
//    const struct State *next[4];    // Next if 2-bit input is 0-3
//};typedef const struct State State_t;
//
//#define Follow &State[0]
//#define Reverso &State[1]
//#define Spinny &State[2]
//#define Stop &State[3]
//
//State_t fsm[4]={
//
//{"State 1", &Follow, {firstState, secondState, thirdState, fourthState}},
//{"State 2", &Reverso, {firstState, secondState, thirdState, fourthState}},
//{"State 3", &Spinny, {firstState, secondState, thirdState, fourthState}},
//{"State 4", &Stop, {firstState, secondState, thirdState, fourthState}},
//};

/**************Initial values used for all programs******************/
#define PWMNOMINAL 5000

volatile uint32_t ControllerFlag; // set every 10ms on controller execution

int32_t Mode = 0;
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time;              // in 0.01 sec



/**************Functions used by all programs***********************/
void Pause3(void){
    int j;
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(200); LaunchPad_Output(0); // off
    Clock_Delay1ms(200); LaunchPad_Output(1); // red
  }
  while(Bump_Read()==0){// wait for touch
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
  }
  for(j=1000;j>100;j=j-200){
    Clock_Delay1ms(j); LaunchPad_Output(0); // off
    Clock_Delay1ms(j); LaunchPad_Output(2); // green
  }
  // restart Jacki
  UR = UL = PWMNOMINAL;    // reset parameters
  Mode = 1;
  ControllerFlag = 0;
  Time = 0;
}

/**************Program17_1******************************************/
// Proportional controller to drive straight with no sensor input
#define DESIRED_SPEED 100
#define TACHBUFF 10                      // number of elements in tachometer array
#define PWMNOMINAL1 5000
#define SWING1 1000
#define PWMIN1 (PWMNOMINAL1-SWING1)
#define PWMAX1 (PWMNOMINAL1+SWING1)

int32_t ActualSpeedL, ActualSpeedR;   	 // Actual speed
int32_t ErrorL, ErrorR;     			 // X* - X'
int32_t PrevErrorL, PrevErrorR;
uint16_t LeftTach[TACHBUFF];             // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;              // direction of left rotation (FORWARD, STOPPED, REVERSE)
int32_t LeftSteps;                       // number of tachometer steps of left wheel (units of 220/360 = 0.61 mm traveled)
uint16_t RightTach[TACHBUFF];            // tachometer period of right wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection RightDir;             // direction of right rotation (FORWARD, STOPPED, REVERSE)
int32_t RightSteps;                      // number of tachometer steps of right wheel (units of 220/360 = 0.61 mm traveled)

// proportional controller gain
// experimentally determine value that creates a stable system
int32_t Ka = 3;
int32_t Kb = 1;

int i = 0;

void LCDClear1(void){
    Nokia5110_Init();
    Nokia5110_Clear();
    Nokia5110_OutString("Desired(RPM)L     R     Actual (RPM)L     R     Error(RPM)  L     R     ");
}
void LCDOut1(void){
    Nokia5110_SetCursor(1, 1);         // one leading space, second row
    Nokia5110_OutUDec(DESIRED_SPEED);
    Nokia5110_SetCursor(7, 1);         // seven leading spaces, second row
    Nokia5110_OutUDec(DESIRED_SPEED);
    Nokia5110_SetCursor(1, 3);       // one leading space, fourth row
    Nokia5110_OutUDec(ActualSpeedL);
    Nokia5110_SetCursor(7, 3);       // seven leading spaces, fourth row
    Nokia5110_OutUDec(ActualSpeedR);
    Nokia5110_SetCursor(0, 5);       // zero leading spaces, sixth row
    Nokia5110_OutSDec(ErrorL);
    Nokia5110_SetCursor(6, 5);       // six leading spaces, sixth row
    Nokia5110_OutSDec(ErrorR);
}

// ------------avg------------
// Simple math function that returns the average
// value of an array.
// Input: array is an array of 16-bit unsigned numbers
//        length is the number of elements in 'array'
// Output: the average value of the array
// Note: overflow is not considered
uint16_t avg(uint16_t *array, int length){
  int i;
  uint32_t sum = 0;
  for(i=0; i<length; i=i+1){
    sum = sum + array[i];
  }
  return (sum/length);
}

//**************************************************
// Proportional controller to drive straight with input
//	from the tachometers
void Controller1(void){

    if(Mode){
		// pull tachometer information
		i = (i+1)%TACHBUFF;
		Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps,
                       &RightTach[i], &RightDir, &RightSteps);

		// use average of ten tachometer values to determine
		// actual speed (similar to Lab16)
		ActualSpeedL = 2000000/ (avg(LeftTach, TACHBUFF));
		ActualSpeedR = 2000000/ (avg(RightTach, TACHBUFF));

		// use proportional control to update duty cycle
		ErrorR = DESIRED_SPEED - ActualSpeedR;
		ErrorL = DESIRED_SPEED - ActualSpeedL;


		UR += Ka*ErrorR - (Kb*PrevErrorR);
		UL += Ka*ErrorL - (Kb*PrevErrorL);

		//PrevErrorR = ErrorR;
		//PrevErrorL = ErrorL;

		// check min/max duty values
		if(UR > PWMAX1) {
		    UR = PWMAX1;
		}
		if(UL > PWMAX1) {
		    UL = PWMAX1;
		}
		if(UR < PWMIN1) {
		    UR = PWMIN1;
		}
		if(UL < PWMIN1) {
		    UL = PWMIN1;
		}

		// update motor values
        Motor_Forward(UL, UR);
		ControllerFlag = 1;
    }
}

// go straight with no sensor input
void Program17_1(void){
  DisableInterrupts();
// initialization
  Clock_Init48MHz();
  LaunchPad_Init();
  Bump_Init();
  Tachometer_Init();
  Motor_Init();

  // user TimerA1 to run the controller at 100 Hz
  // replace this line with a call to TimerA1_Init()
  TimerA1_Init(&Controller1, 5000);

  Motor_Stop();
  UR = UL = PWMNOMINAL;
  EnableInterrupts();
  LCDClear1();
  ControllerFlag = 0;
  Pause3();

	while(1){
		if(Bump_Read()){
			Mode = 0;
			Motor_Stop();
			Pause3();
		}
		if(ControllerFlag){
			LCDOut1();
			ControllerFlag = 0;
		}
  }
}

/**************Program17_2******************************************/
// Proportional controller to drive between two walls using IR sensors
// distances in mm
#define TOOCLOSE 110
#define DESIRED_DIST 172
#define TOOFAR 230
#define PWMNOMINAL2 5000
#define SWING2 1000
#define PWMIN2 (PWMNOMINAL2-SWING2)
#define PWMAX2 (PWMNOMINAL2+SWING2)

volatile uint32_t nr, nc, nl; // raw distance values
int32_t Left, Center, Right; // IR distances in mm
volatile uint32_t ADCflag; // Set every 500us on ADC sample
int32_t DataBuffer[5];
int32_t SetPoint = 172;
uint32_t PosError;
int32_t Error;

// proportional controller gain
// experimentally determine value that creates a stable system
int32_t Kp2 = 40;

void LCDClear2(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("FP: control");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("IR distance");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("L= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("C= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("R= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,5); Nokia5110_OutString("E= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
}

void LCDOut2(void){
  Nokia5110_SetCursor(3,2); Nokia5110_OutSDec(Left);
  Nokia5110_SetCursor(3,3); Nokia5110_OutSDec(Center);
  Nokia5110_SetCursor(3,4); Nokia5110_OutSDec(Right);
  Nokia5110_SetCursor(3,5); Nokia5110_OutSDec(Error);
  // left
  if(Time%5 == 0){
      UART0_OutUDec5(Left);UART0_OutString(" mm,");
      UART0_OutUDec5(Center);UART0_OutString(" mm,");
      UART0_OutUDec5(Right);UART0_OutString(" mm,");
      UART0_OutUDec5(UR);UART0_OutString(" %,");
      UART0_OutUDec5(UL);UART0_OutString(" %,");
      if(Error < 0){
          PosError = Error*(-1);
          UART0_OutString("-");UART0_OutUDec5(PosError);UART0_OutString("\n");
      }
      else{
          UART0_OutUDec5(Error);UART0_OutString("\n");
      }

  }
}

void IRsampling(void){
    uint32_t raw17, raw12, raw16;
    ADC_In17_14_16(&raw17, &raw12, &raw16);
    nr = LPF_Calc(raw17);
    nc = LPF_Calc2(raw12);
    nl = LPF_Calc3(raw16);
    Left = LeftConvert(nl);
    Center = CenterConvert(nc);
    Right = RightConvert(nr);
    ADCflag = 1;
}

/*
* Proportional controller to keep robot in
* center of two walls using IR sensors.
*/
void SysTick_Handler(void){
    if(Mode){
		// Determine set point, middle distance between the walls
        if ((Left > DESIRED_DIST) && (Right > DESIRED_DIST)) {
            SetPoint = (Left + Right) >> 1; //average of left and right
        }
        else {
            SetPoint = DESIRED_DIST;
        }

		// set error based off set point per instruction
//        Error = SetPoint - Right;
        if (Left < Right) {
            Error = Left - SetPoint;
        }
        else {
            Error = SetPoint - Right;
        }

		// update duty cycle based on proportional control
		UR = PWMNOMINAL2 + (Kp2*Error);
		UL = PWMNOMINAL2 + (Kp2*Error);


		// check to ensure not too big of a swing
        if(UR > PWMAX2) {
            UR = PWMAX2;
        }
        if(UL > PWMAX2) {
            UL = PWMAX2;
        }
        if(UR < PWMIN2) {
            UR = PWMIN2;
        }
        if(UL < PWMIN2) {
            UL = PWMIN2;
        }
        else {
        }

		// update motor values
        Motor_Forward(UL, UR);
        ControllerFlag = 1;
    }
}

// proportional control, wall distance
void Program17_2(void){
    uint32_t raw17,raw14,raw16;
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Motor_Init();

	// user TimerA1 to sample the IR sensors at 2000 Hz
	// replace this line with a call to TimerA1_Init()
    TimerA1_Init(&IRsampling, 250);

    Motor_Stop();
    LCDClear2();
    Mode = 0;
    UR = UL = PWMNOMINAL;
    ADCflag = ControllerFlag = 0;   // semaphores

    ADC0_InitSWTriggerCh17_14_16();   // initialize channels 17,12,16
    ADC_In17_14_16(&raw17,&raw14,&raw16);  // sample
    LPF_Init(raw17,64);     // P9.0/channel 17
    LPF_Init2(raw14,64);    // P4.1/channel 12
    LPF_Init3(raw16,64);    // P9.1/channel 16

	// user SysTick to run the controller at 100 Hz with a priority of 2
	// replace this line with a call to SysTick_Init()
    SysTick_Init(4800, 2);

    Pause3();

    EnableInterrupts();
    while(1){
        if(Bump_Read()){ // collision
            Mode = 0;
            Motor_Stop();
            Pause3();
        }
        if(ControllerFlag){ // 100 Hz, not real time
            LCDOut2();
            ControllerFlag = 0;
        }
    }
}

/**************Program17_3******************************************/
#define PWMNOMINAL3 5000
#define SWING3 1000
#define PWMIN3 (PWMNOMINAL3-SWING3)
#define PWMAX3 (PWMNOMINAL3+SWING3)

// Proportional controller to drive robot using line following
uint8_t LineData;       // direct measure from line sensor
int32_t Position;      // position in 0.1mm relative to center of line

// proportional controller gain
// experimentally determine value that creates a stable system
int32_t Kp3= 3;   //maybe 3-5 ?

void LCDClear3(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("17: control");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("Line Follow");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("D =  "); Nokia5110_OutUDec(0);
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("P = "); Nokia5110_OutSDec(0);
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("UR=  "); Nokia5110_OutUDec(0);
  Nokia5110_SetCursor(0,5); Nokia5110_OutString("UL=  "); Nokia5110_OutUDec(0);
}
void LCDOut3(void){
  Nokia5110_SetCursor(5,2); Nokia5110_OutUHex7(LineData);
  Nokia5110_SetCursor(4,3); Nokia5110_OutSDec(Position);
  Nokia5110_SetCursor(5,4); Nokia5110_OutUDec(UR);
  Nokia5110_SetCursor(5,5); Nokia5110_OutUDec(UL);
}

/*
* Proportional controller to drive robot
* using line following
*/
// int32_t change=0;
void Controller3(void){

	// read values from line sensor, similar to
	// SysTick_Handler() in Lab10_Debugmain.c
    if (Time %10 == 0) {
        Reflectance_Start();
    }
    if (Time%10 == 1) {
        // 1 ms later
        // read reflectance into LineData
        LineData = Reflectance_End();
        // Use Reflectance_Position() to find position
        Position = Reflectance_Position(LineData);
    }
    Time = Time + 1;

    if(Mode){

		// update duty cycle based on proportional control
        UR = PWMNOMINAL3 - (Kp3*Position);
		UL = PWMNOMINAL3 + (Kp3*Position);

		//check swing
        if(UR > PWMAX3) {
            UR = PWMAX3;
        }
        if(UL > PWMAX3) {
            UL = PWMAX3;
        }
        if(UR < PWMIN3) {
            UR = PWMIN3;
        }
        if(UL < PWMIN3) {
            UL = PWMIN3;
        }
        Motor_Forward(UL, UR);
		ControllerFlag = 1;
    }
}

// proportional control, line following
void Program17_3(void){
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();

	// user TimerA1 to run the controller at 1000 Hz
	// replace this line with a call to TimerA1_Init()
    TimerA1_Init(&Controller3, 500);

    Motor_Stop();
    LCDClear3();
    Mode = 0;
    Time = 0;
    UR = UL = PWMNOMINAL;
    EnableInterrupts();
    Pause3();
    while(1){
      if(Bump_Read()){ // collision
        Mode = 0;
        Motor_Stop();
        Pause3();
      }
      if(ControllerFlag){ // 100 Hz , not real time
        LCDOut3();
        ControllerFlag = 0;
      }
    }
}

int main(void){
//    Program17_1();
//  Program17_2();
    Program17_3();
}

///*
// * FinalMaze.c
// *
// *  Created on: Nov 27, 2020
// *      Author: C2C Holden Caraway
// */
//
//#include <stdint.h>
//#include "msp.h"
//#include "../inc/Clock.h"
//#include "../inc/CortexM.h"
//#include "../inc/PWM.h"
//#include "../inc/LaunchPad.h"
//#include "../inc/UART0.h"
//#include "../inc/Motor.h"
//#include "../inc/Bump.h"
//#include "../inc/ADC14.h"
//#include "../inc/TimerA1.h"
//#include "../inc/IRDistance.h"
//#include "../inc/Nokia5110.h"
//#include "../inc/LPF.h"
//#include "../inc/SysTickInts.h"
//#include "../inc/Tachometer.h"
//#include "../inc/Reflectance.h"
//
//
//
////// Struct to send a command to the robot
////typedef struct command{
////    uint16_t RightPWM; // 0 to 14998
////    uint16_t LeftPWM; // 0 to 14998
////    void (*MotorFunction)(uint16_t, uint16_t);
////    uint32_t Duration; // time to run in ms
////}command_t;
//
//#define DESIRED_SPEED 100
//#define TACHBUFF 10                      // number of elements in tachometer array
//#define PWMNOMINAL1 5000
//#define SWING1 1000
//#define PWMIN1 (PWMNOMINAL1-SWING1)
//#define PWMAX1 (PWMNOMINAL1+SWING1)
//
//#define TOOCLOSE 110
//#define DESIRED_DIST 172
//#define TOOFAR 230
//#define PWMNOMINAL2 5000
//#define SWING2 1000
//#define PWMIN2 (PWMNOMINAL2-SWING2)
//#define PWMAX2 (PWMNOMINAL2+SWING2)
//
//#define PWMNOMINAL3 5000
//#define SWING3 1000
//#define PWMIN3 (PWMNOMINAL3-SWING3)
//#define PWMAX3 (PWMNOMINAL3+SWING3)
//
//volatile uint32_t nr, nc, nl; // raw distance values
//int32_t Left, Center, Right; // IR distances in mm
//volatile uint32_t ADCflag; // Set every 500us on ADC sample
//int32_t DataBuffer[5];
//int32_t SetPoint = 172;
//uint32_t PosError;
//int32_t ErrorWoo;
//
//int32_t ActualSpeedL, ActualSpeedR;      // Actual speed
//int32_t ErrorL, ErrorR;                  // X* - X'
//int32_t PrevErrorL, PrevErrorR;
//uint16_t LeftTach[TACHBUFF];             // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
//enum TachDirection LeftDir;              // direction of left rotation (FORWARD, STOPPED, REVERSE)
//int32_t LeftSteps;                       // number of tachometer steps of left wheel (units of 220/360 = 0.61 mm traveled)
//uint16_t RightTach[TACHBUFF];            // tachometer period of right wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
//enum TachDirection RightDir;             // direction of right rotation (FORWARD, STOPPED, REVERSE)
//int32_t RightSteps;                      // number of tachometer steps of right wheel (units of 220/360 = 0.61 mm traveled)
//int32_t DesiredSteps;
//
//#define PWMNOMINAL 5000
//
//volatile uint32_t ControllerFlag; // set every 10ms on controller execution
//uint8_t CollisionFlag; // set every 10ms on controller execution
//
//int32_t Mode = 0;
//int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
//uint32_t Time;              // in 0.01 sec
//
//struct State{
//    char name[7];                       // name of state
//    void (*fnctPt)(void);           // function to execute during state
//    const struct State *next[4];    // Next if 2-bit input is 0-3
//};typedef const struct State State_t;
//
//#define Follow &fsm[0]
//#define Reverse &fsm[1]
//#define Spinny &fsm[2]
//#define Stop &fsm[3]
//#define NUM 4
//
//
//
//
//
//State_t *Spt;  // pointer to the current state
//uint32_t Position;
//uint8_t LineData;
//uint8_t Input = 0;
//uint8_t TurningFlag = 0;
//
//// proportional controller gain
//// experimentally determine value that creates a stable system
//
//void Pause3(void){
//    int j;
//  while(Bump_Read()){ // wait for release
//    Clock_Delay1ms(200); LaunchPad_Output(0); // off
//    Clock_Delay1ms(200); LaunchPad_Output(1); // red
//  }
//  while(Bump_Read()==0){// wait for touch
//    Clock_Delay1ms(100); LaunchPad_Output(0); // off
//    Clock_Delay1ms(100); LaunchPad_Output(3); // red/green
//  }
//  while(Bump_Read()){ // wait for release
//    Clock_Delay1ms(100); LaunchPad_Output(0); // off
//    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
//  }
//  for(j=1000;j>100;j=j-200){
//    Clock_Delay1ms(j); LaunchPad_Output(0); // off
//    Clock_Delay1ms(j); LaunchPad_Output(2); // green
//  }
//
//  // restart Jacki
//  UR = UL = PWMNOMINAL;    // reset parameters
//  Mode = 1;
//  ControllerFlag = 0;
//  Time = 0;
//}
//
//
//
//int32_t Kp3= 3;
//void FollowLine (void) {
//
//    uint8_t sensors = 0x00000000;
//    sensors = Bump_Read();
//
//    if (!sensors) {
//        Motor_Stop();            //stop motors
////        sensors = bumpSensor;        //read bump sensors into sensors
//        CollisionFlag = 1;     //set semaphore
//        Spt = Spt->next[1];
//    }
//
//    // read values from line sensor, similar to
//    // SysTick_Handler() in Lab10_Debugmain.c
//    if (Time %10 == 0) {
//        Reflectance_Start();
//    }
//    if (Time%10 == 1) {
//        // 1 ms later
//        // read reflectance into LineData
//        LineData = Reflectance_End();
//        // Use Reflectance_Position() to find position
//        Position = Reflectance_Position(LineData);
//    }
//    Time++;
//
//
//    // update duty cycle based on proportional control
//    UR = PWMNOMINAL3 - (Kp3*Position);
//    UL = PWMNOMINAL3 + (Kp3*Position);
//
//    //check swing
//    if(UR > PWMAX3) {
//        UR = PWMAX3;
//    }
//    if(UL > PWMAX3) {
//        UL = PWMAX3;
//    }
//    if(UR < PWMIN3) {
//        UR = PWMIN3;
//    }
//    if(UL < PWMIN3) {
//        UL = PWMIN3;
//    }
//    Motor_Forward(UL, UR);
//    ControllerFlag = 1;
//
//    Spt = Spt->next[Input];
//}
//
//void Backup (void) {
//
//    //how to implement this in an if statement
//    for (Time; Time < 150; Time++) {
//        Motor_Backward(UR, UL);
//    }
//
//    Spt = Spt->next[2];
//
//}
//
//void Turning (void) {
//
//    UL = UR = PWMNOMINAL1;
//    DesiredSteps = LeftSteps + (180*2);
//
//    Tachometer_Get(&LeftTach[0], &LeftDir, &LeftSteps,
//                   &RightTach[0], &RightDir, &RightSteps);
//
//    while (LeftSteps < DesiredSteps) {
//        Motor_Right (UL, UR);
//        Tachometer_Get(&LeftTach[0], &LeftDir, &LeftSteps,
//                       &RightTach[0], &RightDir, &RightSteps);
//    }
//
//    Motor_Stop();
//    ControllerFlag = 1;
//    TurningFlag = 1;
//    Spt = Spt->next[Input];
//}
//
//void getFlashed (void) {
//    Time++;
//    //flash red
//    if(Time % 10000 == 0) {
//        LaunchPad_Output(1);
//    }
//    //flash off
//    if(Time % 20000 == 0) {
//        LaunchPad_Output(0);
//    }
//    //flash blue
//    if(Time % 30000 == 0) {
//        LaunchPad_Output(4);
//    }
//    //flash off
//    if(Time % 40000 == 0) {
//        LaunchPad_Output(0);
//    }
//}
//
//
//void Halt (void) {
//
//    Motor_Stop();
//    //flash lights or something idk
//    while (1) {
//    getFlashed();
//    }
////    Spt = Spt->next[Input];
//}
//
//int32_t Ka = 3;
//int32_t Kb = 1;
//
//int i = 0;
//
//void LCDClear1(void){
//    Nokia5110_Init();
//    Nokia5110_Clear();
//    Nokia5110_OutString("Desired(RPM)L     R     Actual (RPM)L     R     Error(RPM)  L     R     ");
//}
//void LCDOut1(void){
//    Nokia5110_SetCursor(1, 1);         // one leading space, second row
//    Nokia5110_OutUDec(DESIRED_SPEED);
//    Nokia5110_SetCursor(7, 1);         // seven leading spaces, second row
//    Nokia5110_OutUDec(DESIRED_SPEED);
//    Nokia5110_SetCursor(1, 3);       // one leading space, fourth row
//    Nokia5110_OutUDec(ActualSpeedL);
//    Nokia5110_SetCursor(7, 3);       // seven leading spaces, fourth row
//    Nokia5110_OutUDec(ActualSpeedR);
//    Nokia5110_SetCursor(0, 5);       // zero leading spaces, sixth row
//    Nokia5110_OutSDec(ErrorL);
//    Nokia5110_SetCursor(6, 5);       // six leading spaces, sixth row
//    Nokia5110_OutSDec(ErrorR);
//}
//
//State_t fsm[4]={
//{"Follow", &FollowLine, {Follow, Stop, Spinny, Stop}},
//{"Reverse", &Backup, {Reverse, Reverse, Spinny, Reverse}},
//{"Spinny", &Turning, {Spinny, Reverse, Follow, Reverse}},
//{"Stop", &Halt, {Stop, Stop, Stop, Stop}},
//};
//
//void Controller1(void){
//
//    Input = 0;
//    if(Bump_Read()) {
//        Input = 1;
//    }
//
//    Time ++;
//    // read values from line sensor, similar to
//    if (Time %10 == 0) {
//        Reflectance_Start();
//    }
//    if (Time%10 == 1) {
//        // 1 ms later
//        // read reflectance into LineData
//        LineData = Reflectance_End();
//        // Use Reflectance_Position() to find position
//        Position = Reflectance_Position(LineData);
//    }
//    Input = (TurningFlag << 1) | Input;
//    Spt = Spt->next[Input];
////    Spt->fnctPt();
//    ControllerFlag = 1;
//   }
//
//void Level_1(void){
//  DisableInterrupts();
//// initialization
//  Clock_Init48MHz();
//  LaunchPad_Init();
//  Bump_Init();
//  Reflectance_Init();
//  Tachometer_Init();
//  Motor_Init();
//
//  // user TimerA1 to run the controller at 100 Hz
//  // replace this line with a call to TimerA1_Init()
//  TimerA1_Init(&Controller1, 5000);     //would this be 500? maybe
//
//  Motor_Stop();
//  UR = UL = PWMNOMINAL;
//  EnableInterrupts();
//  LCDClear1();
//  Spt = Follow;
//  EnableInterrupts();
////  ControllerFlag = 0;
////  Pause3();
//
//    while(1){
//        Spt->fnctPt();
//        if(Bump_Read()){
// //           Mode = 0;
//            Motor_Stop();
// //           Pause3();
//        }
//        if(ControllerFlag){
//            LCDOut1();
//            ControllerFlag = 0;
//        }
//  }
//}
//
//
//int32_t Kp2 = 40;
//
//void SysTick_Handler(void){
//
//scenario_t theReturn;
//
//    if(Mode){
//
//        // Determine set point, middle distance between the walls
//        if ((Left > DESIRED_DIST) && (Right > DESIRED_DIST)) {
//            SetPoint = (Left + Right) >> 1; //average of left and right
//        }
//        else {
//            SetPoint = DESIRED_DIST;
//        }
//
//        // set error based off set point per instruction
////        Error = SetPoint - Right;
//        if (Left < Right) {
//            ErrorWoo = Left - SetPoint;
//        }
//        else {
//            ErrorWoo = SetPoint - Right;
//        }
//
//        //C2C DAVID SONG
//        theReturn = Classify(Left, Center, Right);
//
//        if (theReturn == LeftJoint) {
//            Left = DESIRED_DIST;
//        }
//
//        if (theReturn == TeeJoint) {
//            Left = DESIRED_DIST;
//        }
//
//        // update duty cycle based on proportional control
//        UR = PWMNOMINAL2 + (Kp2*ErrorWoo);
//        UL = PWMNOMINAL2 + (Kp2*ErrorWoo);
//
//
//        // check to ensure not too big of a swing
//        if(UR > PWMAX2) {
//            UR = PWMAX2;
//        }
//        if(UL > PWMAX2) {
//            UL = PWMAX2;
//        }
//        if(UR < PWMIN2) {
//            UR = PWMIN2;
//        }
//        if(UL < PWMIN2) {
//            UL = PWMIN2;
//        }
//        else {
//        }
//
//        // update motor values
//        Motor_Forward(UL, UR);
//        ControllerFlag = 1;
//    }
//}
//
//void LCDClear2(void){
//  Nokia5110_Init();
//  Nokia5110_Clear(); // erase entire display
//  Nokia5110_OutString("17: control");
//  Nokia5110_SetCursor(0,1); Nokia5110_OutString("IR distance");
//  Nokia5110_SetCursor(0,2); Nokia5110_OutString("L= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
//  Nokia5110_SetCursor(0,3); Nokia5110_OutString("C= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
//  Nokia5110_SetCursor(0,4); Nokia5110_OutString("R= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
//  Nokia5110_SetCursor(0,5); Nokia5110_OutString("E= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
//}
//
//void LCDOut2(void){
//  Nokia5110_SetCursor(3,2); Nokia5110_OutSDec(Left);
//  Nokia5110_SetCursor(3,3); Nokia5110_OutSDec(Center);
//  Nokia5110_SetCursor(3,4); Nokia5110_OutSDec(Right);
//  Nokia5110_SetCursor(3,5); Nokia5110_OutSDec(ErrorWoo);
//  // left
//  if(Time%5 == 0){
//      UART0_OutUDec5(Left);UART0_OutString(" mm,");
//      UART0_OutUDec5(Center);UART0_OutString(" mm,");
//      UART0_OutUDec5(Right);UART0_OutString(" mm,");
//      UART0_OutUDec5(UR);UART0_OutString(" %,");
//      UART0_OutUDec5(UL);UART0_OutString(" %,");
//      if(ErrorWoo < 0){
//          PosError = ErrorWoo*(-1);
//          UART0_OutString("-");UART0_OutUDec5(PosError);UART0_OutString("\n");
//      }
//      else{
//          UART0_OutUDec5(ErrorWoo);UART0_OutString("\n");
//      }
//
//  }
//}
//
//void IRsampling(void){
//    uint32_t raw17, raw12, raw16;
//    ADC_In17_14_16(&raw17, &raw12, &raw16);
//    nr = LPF_Calc(raw17);
//    nc = LPF_Calc2(raw12);
//    nl = LPF_Calc3(raw16);
//    Left = LeftConvert(nl);
//    Center = CenterConvert(nc);
//    Right = RightConvert(nr);
//    ADCflag = 1;
//}
//
//void Program17_2(void){
//    uint32_t raw17,raw14,raw16;
//    DisableInterrupts();
//    Clock_Init48MHz();
//    LaunchPad_Init();
//    Bump_Init();
//    Motor_Init();
//
//    // user TimerA1 to sample the IR sensors at 2000 Hz
//    // replace this line with a call to TimerA1_Init()
//    TimerA1_Init(&IRsampling, 250);
//
//    Motor_Stop();
//    LCDClear2();
//    Mode = 0;
//    UR = UL = PWMNOMINAL;
//    ADCflag = ControllerFlag = 0;   // semaphores
//
//    ADC0_InitSWTriggerCh17_14_16();   // initialize channels 17,12,16
//    ADC_In17_14_16(&raw17,&raw14,&raw16);  // sample
//    LPF_Init(raw17,64);     // P9.0/channel 17
//    LPF_Init2(raw14,64);    // P4.1/channel 12
//    LPF_Init3(raw16,64);    // P9.1/channel 16
//
//    // user SysTick to run the controller at 100 Hz with a priority of 2
//    // replace this line with a call to SysTick_Init()
//    SysTick_Init(4800, 2);
//
//    Pause3();
//
//    EnableInterrupts();
//    while(1){
//        if(Bump_Read()){ // collision
//            Mode = 0;
//            Motor_Stop();
//            Pause3();
//        }
//        if(ControllerFlag){ // 100 Hz, not real time
//            LCDOut2();
//            ControllerFlag = 0;
//        }
//    }
//}
//
//void LCDClear3(void){
//  Nokia5110_Init();
//  Nokia5110_Clear(); // erase entire display
//  Nokia5110_OutString("17: control");
//  Nokia5110_SetCursor(0,1); Nokia5110_OutString("Line Follow");
//  Nokia5110_SetCursor(0,2); Nokia5110_OutString("D =  "); Nokia5110_OutUDec(0);
//  Nokia5110_SetCursor(0,3); Nokia5110_OutString("P = "); Nokia5110_OutSDec(0);
//  Nokia5110_SetCursor(0,4); Nokia5110_OutString("UR=  "); Nokia5110_OutUDec(0);
//  Nokia5110_SetCursor(0,5); Nokia5110_OutString("UL=  "); Nokia5110_OutUDec(0);
//}
//void LCDOut3(void){
//  Nokia5110_SetCursor(5,2); Nokia5110_OutUHex7(LineData);
//  Nokia5110_SetCursor(4,3); Nokia5110_OutSDec(Position);
//  Nokia5110_SetCursor(5,4); Nokia5110_OutUDec(UR);
//  Nokia5110_SetCursor(5,5); Nokia5110_OutUDec(UL);
//}
//
//
//
//
//void Program17_3(void){
//    DisableInterrupts();
//    Clock_Init48MHz();
//    LaunchPad_Init();
//    Bump_Init();
//    Reflectance_Init();
//    Motor_Init();
//
//    // user TimerA1 to run the controller at 1000 Hz
//    // replace this line with a call to TimerA1_Init()
//    TimerA1_Init(&FollowLine, 500);
//
//    Motor_Stop();
//    LCDClear3();
//    Mode = 0;
//    Time = 0;
//    UR = UL = PWMNOMINAL;
//    EnableInterrupts();
//    Pause3();
//    while(1){
//      if(Bump_Read()){ // collision
//        Mode = 0;
//        Motor_Stop();
//        Pause3();
//      }
//      if(ControllerFlag){ // 100 Hz , not real time
//        LCDOut3();
//        ControllerFlag = 0;
//      }
//    }
//}
//
//#define TOOCLOSE 110
//#define DESIRED_DIST 172
//#define TOOFAR 230
//#define PWMNOMINAL2 5000
//#define SWING2 1000
//#define PWMIN2 (PWMNOMINAL2-SWING2)
//#define PWMAX2 (PWMNOMINAL2+SWING2)
//
////volatile uint32_t nr, nc, nl; // raw distance values
//////int32_t Left, Center, Right; // IR distances in mm
////volatile uint32_t ADCflag; // Set every 500us on ADC sample
////int32_t DataBuffer[5];
////int32_t SetPoint = 172;
////uint32_t PosError;
////int32_t Error;
//
//// proportional controller gain
//// experimentally determine value that creates a stable system
//
//
//int main() {
//    Level_1();
////    Level_2();
////    Level_3();
////    Level_4();
//}
//
//
