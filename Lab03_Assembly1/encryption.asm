; Encryption.asm
; Runs on any Cortex M
; Student name: Holden Caraway
; Date:24 Aug 2020
; Basic XOR encryption and decryption functions for cryptography.
; Capt Steven Beyer
; July 23 2020
;
;Simplified BSD License (FreeBSD License)
;Copyright (c) 2020, Steven Beyer, All rights reserved.
;
;Redistribution and use in source and binary forms, with or without modification,
;are permitted provided that the following conditions are met:
;
;1. Redistributions of source code must retain the above copyright notice,
;   this list of conditions and the following disclaimer.
;2. Redistributions in binary form must reproduce the above copyright notice,
;   this list of conditions and the following disclaimer in the documentation
;   and/or other materials provided with the distribution.
;
;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
;AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
;USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;The views and conclusions contained in the software and documentation are
;those of the authors and should not be interpreted as representing official
;policies, either expressed or implied, of the FreeBSD Project.


       .thumb
       .text
       .align 2
       .global Encrypt			;encrypts the messages
       .global Decrypt			;decrypts the messages
       .global XOR_bytes		;XORs the bytes passed to it and stores result in register

;------------Encrypt------------
; Takes in the location of a plain text message and key and
;	xors the message to encrypt it. It stores the
;	result at a passed in memory location. Stops
;	when the ASCII character # is encountered.
; Input: R0 is message address, R1 is key address,
;	R2 is location to store encrypted message
; Output: encrypted message stored at memory
;	located at R2
; Modifies: R0, R1

Encrypt:   .asmfunc

	; If a function wishes to use R4-R11 it must save and restore them using the stack.

	PUSH {R3, LR}			; will be pushing to the stack to save the registers

	MOV R4, R0				; moving address of R0 into R4

	LDR R1, [R1]			; loading R1 with value at R1

	AND R1, R1, #0x000000FF 	; select first byte of key and clear the rest of the bytes

	LDR R0, [R0]			;loads value at R0 into address of R0



encryptionLoop		; loopty loop

	LDRB R3, [R4], #1	; retrieve next byte of message

	PUSH {LR} 			; save registers

	BL XOR_bytes		; XOR_Bytes

	POP {LR}			; keeping LR alive

	STRB R5, [R2]		; storing the value at R2 into R5

	ADD R2, R2, #1		; grabbing the next byte in the message by incrementing by 1

	CMP R3, #35		; compare byte from message with end of message

	BNE encryptionLoop		; loop or end

	PUSH {LR}				; restore messages

	BX LR 					;returns to main function using LR address

		.endasmfunc

;------------Decrypt------------
; Takes in the location of an encrypted message and key and
;	xors the message to decrypt it it. It stores the
;	result at a passed in memory location. Stops
;	when the ASCII character # is encountered.
; Input: R0 is message address, R1 is key address,
;	R2 is location to store decrypted message
; Output: decrypted message stored at memory
;	located ate R2
; Modifies: R0, R1


Decrypt:	.asmfunc

	PUSH {R3, LR}			; save registers

	MOV R4, R0				; moving address of R0 into R4

	LDR R1, [R1] 		    ; load value at R1 into R1 address

	AND R1, R1, #0x000000FF	; selecting first byte of the key and clearing the rest


decryptionLoop			; loopty loop

	PUSH {LR}			;pushes LR to the stack to save it

	LDRB R3, [R4], #1	; retrieve next byte of message

	BL XOR_bytes		; breaks to XOR_Bytes

	STRB R5, [R2], #1	; stores a byte of R5 into the address of R2

	POP {LR}			; restore messages

	CMP R5, #0x23		; compare byte from decrypted with end of message

	BNE decryptionLoop	; loop or end


	BX LR

		.endasmfunc

;------------XOR_Bytes------------
; Takes in two bytes, XORs them, returns the result
; Input: R0 message byte, R1 is key byte,
; Output: R0 is XOR result
; Modifies: R0

XOR_bytes:	.asmfunc

	; eor

	EOR R5, R3, R1 		;XORing R1 and R3, storing in R5. Supposed to be R5

	BX LR 				;Breaking to location in LR

		.endasmfunc

		.align 4

    .end
