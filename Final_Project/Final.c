/*
 * FinalMaze.c
 *
 *  Created on: Nov 27, 2020
 *      Author: C2C Holden Caraway
 *
Date: 27 November 2020
Instructor/Student Who Helped: Maj Birrer
Help Received: Maj Birrer assisted during an EI session by providing clarifying answers in
            regard to my questions about FSM implementation. We did some
            initial troubleshooting of the FSM so that I had a foundational
            knowledge of how the FSM would be used in Level 1 and how
            the transitions would be determined using Input and TurningFlag.

Date: 29 November 2020
Instructor/Student Who Helped: C2C D. Song
Help Received: C2C assisted on Level 4 by recommending I add if statements to compare a
            variable set to Classify() to check if the return is equal to the paths
            seen on the maze (and which I was encountering problems on) and
            set variables to correct for these scenarios that I wanted the robot to
            ignore. I did so, and while it seems I had the correct solution, other
            factors unknown to me have prevented Level 4 from accomplishing
            full functionality.


Date: 2 December 2020
Instructor/Student Who Helped: Maj Birrer
Help Received: Major Birrer assisted during an EI session by troubleshooting my FSM
        and correcting my use of the TurningFlag semaphore as well as how
        my next state was being set within my Controller1(), which led to a
        frustrating but equally funny story. He also assisted during this EI session
        by helping me fix my Level 3 which was not turning appropriately upon bump.
        It was found to be my bump variable was only calling the turn function once
        (which is not noticeable in real-time) and solved it by implementing a global
        variable �bumpy� which was set to 1 upon bump and throughout the turn and
        set back to 0 after the turn was complete.

Date: 4 December 2020
Instructor/Student Who Helped: C2C A. Lee
Help Received: C2C Lee assisted by recommending I delete the fourth of my FSM in Level 2
            (which was a replica of Level 1 FSM) in order to prevent it from going
            to the infinite stop and flash simply after two bumps. I did so and this fixed
            the issue of stopping/flashing after just two bumps but my Level 2 did not
            get any further functionality due to an FSM transition problem I have not
            been able to correct and will not have the time to do so.
 *
 */



#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"
#include "../inc/Classifier.h"

//LEVEL1
#define TOOCLOSE 110
#define DESIRED_DIST 144
#define TOOFAR 240
#define PWMNOMINAL1 5000
#define SWING1 1100 //1750
#define PWMIN1 (PWMNOMINAL1-SWING1)
#define PWMAX1 (PWMNOMINAL1+SWING1)
//LEVEL2
#define PWMNOMINAL2 5000 //3400
#define SWING2 1900 //1350
#define PWMIN2 (PWMNOMINAL2-SWING2)
#define PWMAX2 (PWMNOMINAL2+SWING2)
//LEVEL3
#define DESIRED_SPEED 100
#define TACHBUFF 10                      // number of elements in tachometer array
#define PWMNOMINAL3 8000
#define SWING3 2000
#define PWMIN3 (PWMNOMINAL3-SWING3)
#define PWMAX3 (PWMNOMINAL3+SWING3)
//LEVEL4
#define TOOCLOSE 110
#define DESIRED_DIST 144
#define TOOFAR 240
#define PWMNOMINAL4 5000
#define SWING4 2000 //1750
#define PWMIN4 (PWMNOMINAL4-SWING4)
#define PWMAX4 (PWMNOMINAL4+SWING4)

#define botRadius 60
#define wheelRadius 35

#define PWMNOMINAL 5000

int startSteps = 0;  //initialize first step of tach


volatile uint32_t nr, nc, nl; // raw distance values
int32_t Left, Center, Right; // IR distances in mm
volatile uint32_t ADCflag; // Set every 500us on ADC sample
int32_t DataBuffer[5];
int32_t SetPoint = 172;
uint32_t PosError;
int32_t ErrorWoo;

int32_t ActualSpeedL, ActualSpeedR;      // Actual speed
int32_t ErrorL, ErrorR;                  // X* - X'
int32_t PrevErrorL, PrevErrorR;
uint16_t LeftTach[TACHBUFF];             // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;              // direction of left rotation (FORWARD, STOPPED, REVERSE)
int32_t LeftSteps;                       // number of tachometer steps of left wheel (units of 220/360 = 0.61 mm traveled)
uint16_t RightTach[TACHBUFF];            // tachometer period of right wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection RightDir;             // direction of right rotation (FORWARD, STOPPED, REVERSE)
int32_t RightSteps;                      // number of tachometer steps of right wheel (units of 220/360 = 0.61 mm traveled)
int32_t DesiredSteps;

uint8_t bumpy = 0;

volatile uint32_t ControllerFlag; // set every 10ms on controller execution
uint8_t CollisionFlag; // set every 10ms on controller execution

int32_t Mode = 0;
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time;              // in 0.01 sec

struct State{
    char name[8];                       // name of state
    void (*fnctPt)(void);           // function to execute during state
    const struct State *next[4];    // Next if 2-bit input is 0-3
};typedef const struct State State_t;

#define Follow &fsm[0]
#define Reverse &fsm[1]
#define Spinny &fsm[2]
#define Stop &fsm[3]
#define NUM 4

State_t *Spt;  // pointer to the current state
uint32_t Position;
uint8_t LineData;
uint8_t Input = 0;
uint8_t TurningFlag = 0;

struct twoState{
    char name[13];                       // name of state
    void (*twofnctPt)(void);           // function to execute during state
    const struct twoState *next[3];    // Next if 2-bit input is 0-3        CHANGED TO 3
};typedef const struct twoState twoState_t;

#define twoFollow &twofsm[0]
#define twoReverse &twofsm[1]
#define twoSpinny &twofsm[2]
//#define twoStop &twofsm[3]

twoState_t *twoSpt;  // pointer to the current state

void Pause3(void){
    int j;
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(200); LaunchPad_Output(0); // off
    Clock_Delay1ms(200); LaunchPad_Output(1); // red
  }
  while(Bump_Read()==0){// wait for touch
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
  }
  for(j=1000;j>100;j=j-200){
    Clock_Delay1ms(j); LaunchPad_Output(0); // off
    Clock_Delay1ms(j); LaunchPad_Output(2); // green
  }
  UR = UL = PWMNOMINAL;    // reset parameters
  Mode = 1;
  ControllerFlag = 0;
  Time = 0;
}

int32_t Kp1= 33;
void FollowLine (void) {

    UR = PWMNOMINAL1 - (Kp1*Position);
    UL = PWMNOMINAL1 + (Kp1*Position);

    //check swing
    if(UR > PWMAX1) {
        UR = PWMAX1;
    }
    if(UL > PWMAX1) {
        UL = PWMAX1;
    }
    if(UR < PWMIN1) {
        UR = PWMIN1;
    }
    if(UL < PWMIN1) {
        UL = PWMIN1;
    }
    Motor_Forward(UR, UL);
    ControllerFlag = 1;
    //Spt = Spt->next[Input];
}

int BackSteps = 250;
void Backup (void) {

    UL = UR = PWMNOMINAL1;
    if (startSteps == 0) {
        startSteps = LeftSteps; //first time doing backup will init startSteps as LeftSteps
    }
    if(startSteps < (LeftSteps + BackSteps)) {
        Motor_Backward(UR, UL);
    }
    else {
        startSteps = 0;
        Spt = Spt->next[2];
        Motor_Stop();
    }
}

void Turning (void) {
    //rightTurn();
    int degrees = 300;
    UL = UR = PWMNOMINAL1;
    DesiredSteps = (degrees) * (botRadius/wheelRadius);
    if (startSteps == 0) {
        startSteps = LeftSteps; //first time doing backup will init startSteps as LeftSteps
    }
    if(LeftSteps < (startSteps + DesiredSteps)) {
        Motor_Right(UR, UL);
    }
    else {
        startSteps = 0;
        TurningFlag = 1;
        Motor_Stop();
    }
}

void getFlashed (void) {
    Time++;
    //flash red
    if(Time % 10000 == 0) {
        LaunchPad_Output(1);
    }
    //flash off
    if(Time % 30000 == 0) {
        LaunchPad_Output(0);
    }
    //flash blue
    if(Time % 50000 == 0) {
        LaunchPad_Output(4);
    }
    //flash off
    if(Time % 70000 == 0) {
        LaunchPad_Output(0);
    }
}

void Halt (void) {

    Motor_Stop();
    //flash lights or something idk
    while (1) {
    getFlashed();
    }
}


State_t fsm[4]={
{"Follow", &FollowLine, {Follow, Reverse, Follow, Stop}},
{"Reverse", &Backup, {Reverse, Reverse, Spinny, Reverse}},
{"Spinny", &Turning, {Spinny, Spinny, Follow, Stop}},
{"Stop", &Halt, {Stop, Stop, Stop, Stop}},
};

State_t *Spt = Follow;

int32_t Ka = 3;
int32_t Kb = 1;

int i = 0;
void Controller1(void){
    int bumpBit = 0;
    Time++;
    Input = 0;
    // read values from line sensor, similar to
    if (Time %10 == 0) {
        Reflectance_Start();
    }
    if (Time%10 == 1) {
        // 1 ms later
        // read reflectance into LineData
        LineData = Reflectance_End();
        // Use Reflectance_Position() to find position
        Position = Reflectance_Position(LineData);
    }
    Tachometer_Get(&LeftTach[0], &LeftDir, &LeftSteps, &RightTach[0], &RightDir, &RightSteps);
    if(Bump_Read()) {
//      Input = 1;
        bumpBit = 1;
        }
    Input = ((TurningFlag << 1) | bumpBit);
    Spt = Spt->next[Input];
    Spt->fnctPt();
    ControllerFlag = 1;
//    Time++;
}

void Level_1(void){
  DisableInterrupts();
// initialization
  Clock_Init48MHz();
  LaunchPad_Init();
  Bump_Init();
  Reflectance_Init();
  Tachometer_Init();
  Motor_Init();

  // user TimerA1 to run the controller at 100 Hz
  // replace this line with a call to TimerA1_Init()
  TimerA1_Init(&Controller1, 1000);
  Motor_Stop();
  Time = 0;
  Spt = Follow;
  EnableInterrupts();
//  ControllerFlag = 0;
    while(1) {
    }
}

int reverseSteps = 500;
void backItUpTerry(void) {

    UL = UR = PWMNOMINAL1;
    if (startSteps == 0) {
        startSteps = LeftSteps; //first time doing backup will init startSteps as LeftSteps
    }
    if(startSteps < (LeftSteps + reverseSteps)) {
        Motor_Backward(UR, UL);
    }
    else {
        startSteps = 0;
        Motor_Stop();
    }
}

void right75 (void) {
    int degrees = 140;
    UL = UR = PWMNOMINAL1;
    DesiredSteps = (degrees) * (botRadius/wheelRadius);
    if (startSteps == 0) {
        startSteps = LeftSteps; //first time doing backup will init startSteps as LeftSteps
    }
    if(LeftSteps < (startSteps + DesiredSteps)) {
        Motor_Right(UR, UL);
    }
    else {
        bumpy = 0;
        startSteps = 0;
        Motor_Stop();
    }
}

void left75 (void) {
    int degrees = 140;
    UL = UR = PWMNOMINAL1;
    DesiredSteps = (degrees) * (botRadius/wheelRadius);
    if (startSteps == 0) {
        startSteps = RightSteps; //first time doing backup will init startSteps as LeftSteps
    }
    if(RightSteps < (startSteps + DesiredSteps)) {
        Motor_Left(UR, UL);
    }
    else {
        bumpy = 0;
        startSteps = 0;
        Motor_Stop();
    }

}
void right90 (void) {
    int degrees = 160;
    UL = UR = PWMNOMINAL1;
    DesiredSteps = (degrees) * (botRadius/wheelRadius);
    if (startSteps == 0) {
        startSteps = LeftSteps; //first time doing backup will init startSteps as LeftSteps
    }
    if(LeftSteps < (startSteps + DesiredSteps)) {
        Motor_Right(UR, UL);
    }
    else {
        bumpy = 0;
        startSteps = 0;
        Motor_Stop();
    }
}

void left90 (void) {
    int degrees = 160;
    UL = UR = PWMNOMINAL1;
    DesiredSteps = (degrees) * (botRadius/wheelRadius);
    if (startSteps == 0) {
        startSteps = RightSteps; //first time doing backup will init startSteps as LeftSteps
    }
    if(RightSteps < (startSteps + DesiredSteps)) {
        Motor_Left(UR, UL);
    }
    else {
        bumpy = 0;
        startSteps = 0;
        Motor_Stop();
    }
}

int32_t twoKp3= 7;
void twoFollowLine (void) {

    UR = PWMNOMINAL2 - (twoKp3*Position);
    UL = PWMNOMINAL2 + (twoKp3*Position);

    //check swing
    if(UR > PWMAX2) {
        UR = PWMAX2;
    }
    if(UL > PWMAX2) {
        UL = PWMAX2;
    }
    if(UR < PWMIN2) {
        UR = PWMIN2;
    }
    if(UL < PWMIN2) {
        UL = PWMIN2;
    }
    Motor_Forward(UR, UL);
    ControllerFlag = 1;
    //Spt = Spt->next[Input];
}

int twoBackSteps = 150;
void twoBackup (void) {

    UL = UR = PWMNOMINAL2;
    if (startSteps == 0) {
        startSteps = LeftSteps;         //first time doing backup will init startSteps as LeftSteps
    }
    if(startSteps < (LeftSteps + BackSteps)) {
        Motor_Backward(UR, UL);
    }
    else {
        startSteps = 0;
        twoSpt = twoSpt->next[2];
        Motor_Stop();
    }
}

void twoTurning (void) {
    //rightTurn();
    //TurningFlag = 0;
    int degrees = 115;
    UL = UR = PWMNOMINAL2;
    DesiredSteps = (degrees) * (botRadius/wheelRadius);
    if (startSteps == 0) {
        startSteps = RightSteps; //first time doing backup will init startSteps as LeftSteps
    }
    if(RightSteps < (startSteps + DesiredSteps)) {
        Motor_Left(UR, UL);
    }
    else {
        startSteps = 0;
        TurningFlag = 1;
        Motor_Stop();
    }
}

void twoFlashed (void) {
    Time++;
    //flash red
    if(Time % 10000 == 0) {
        LaunchPad_Output(1);
    }
    //flash off
    if(Time % 30000 == 0) {
        LaunchPad_Output(0);
    }
    //flash blue
    if(Time % 50000 == 0) {
        LaunchPad_Output(4);
    }
    //flash off
    if(Time % 70000 == 0) {
        LaunchPad_Output(0);
    }
}

void twoHalt (void) {

    Motor_Stop();
    //flash lights
    while (1) {
    twoFlashed();
    }
}

twoState_t twofsm[3]={
{"twoFollow", &twoFollowLine, {twoFollow, twoReverse, twoReverse}}, //twoFollow third param
{"twoReverse", &twoBackup, {twoReverse, twoReverse, twoSpinny}},
{"twoSpinny", &twoTurning, {twoSpinny, twoSpinny, twoFollow}},
};

twoState_t *twoSpt = twoFollow;
void Controller2(void){

    int twobumpBit = 0;
    Time++;
    Input = 0;
    // read values from line sensor, similar to
    if (Time %10 == 0) {
        Reflectance_Start();
    }
    if (Time%10 == 1) {
        // 1 ms later
        // read reflectance into LineData
        LineData = Reflectance_End();
        // Use Reflectance_Position() to find position
        Position = Reflectance_Position(LineData);
    }
    if(LineData == 219) {
        twoHalt();
    }
    Tachometer_Get(&LeftTach[0], &LeftDir, &LeftSteps, &RightTach[0], &RightDir, &RightSteps);

    if(Bump_Read()) {
//      Input = 1;
        twobumpBit = 1;
        }
    Input = ((TurningFlag << 1) | twobumpBit);
    twoSpt = twoSpt->next[Input];
    twoSpt->twofnctPt();
    ControllerFlag = 1;
}

void Level_2(void){
    DisableInterrupts();
  // initialization
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Tachometer_Init();
    Motor_Init();

    // user TimerA1 to run the controller at 100 Hz
    // replace this line with a call to TimerA1_Init()
    TimerA1_Init(&Controller2, 1000);

    Motor_Stop();

    Time = 0;
    twoSpt = twoFollow;

    EnableInterrupts();
    ControllerFlag = 0;

      while(1) {
      }
}


void IRsampling(void){
    uint32_t raw17, raw12, raw16;
    ADC_In17_14_16(&raw17, &raw12, &raw16);
    nr = LPF_Calc(raw17);
    nc = LPF_Calc2(raw12);
    nl = LPF_Calc3(raw16);
    Left = LeftConvert(nl);
    Center = CenterConvert(nc);
    Right = RightConvert(nr);
    ADCflag = 1;
}

void Controller3 (void) {

    UR = UL = PWMNOMINAL1;
    Tachometer_Get(&LeftTach[0], &LeftDir, &LeftSteps, &RightTach[0], &RightDir, &RightSteps);

    if(Bump_Read()) {
        bumpy = Bump_Read();
    }
    if((bumpy == 25) || (bumpy == 13)) { //stuck in right side corner
        left90();
    }
    else if((bumpy == 38) || (bumpy == 44)) {  //stuck in left side corner
        right90();
    }
    else if(bumpy == 12) {    //head on collision
        left90();
    }

    else if(bumpy > 7) {    //hit on left side
        right75();
    }
    else if((bumpy < 8) & (bumpy > 0)) {        //hit on right side
        left75();
    }
    else {
        Motor_Forward(UR, UL);
    }
}
void Level_3(void){
    DisableInterrupts();
  // initialization
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Tachometer_Init();
    Motor_Init();

    // user TimerA1 to run the controller at 100 Hz
    // replace this line with a call to TimerA1_Init()
    TimerA1_Init(&Controller3, 500);

    Motor_Stop();
    Time = 0;
    UR = UL = PWMNOMINAL;
    EnableInterrupts();

      while(1){

      }
}

int32_t Kp2 = 350;

void SysTick_Handler(void){

scenario_t theReturn;

    if(Mode){
        // Determine set point, middle distance between the walls
        if ((Left > DESIRED_DIST) && (Right > DESIRED_DIST)) {
            SetPoint = (Left + Right) >> 1; //average of left and right
        }
        else {
            SetPoint = DESIRED_DIST;
        }

        // set error based off set point per instruction
//        Error = SetPoint - Right;
        if (Left < Right) {
            ErrorWoo = Left - SetPoint;
        }
        else {
            ErrorWoo = SetPoint - Right;
        }

        //C2C DAVID SONG
        theReturn = Classify(Left, Center, Right);

        if (theReturn == LeftJoint) {
            Left = Right;
        }

        if (theReturn == TeeJoint) {
            Left = DESIRED_DIST;
        }

        // update duty cycle based on proportional control
        UR = PWMNOMINAL2 - (Kp2*ErrorWoo);
        UL = PWMNOMINAL2 + (Kp2*ErrorWoo);


        // check to ensure not too big of a swing
        if(UR > PWMAX2) {
            UR = PWMAX2;
        }
        if(UL > PWMAX2) {
            UL = PWMAX2;
        }
        if(UR < PWMIN2) {
            UR = PWMIN2;
        }
        if(UL < PWMIN2) {
            UL = PWMIN2;
        }
        else {
        }
        // update motor values
        Motor_Forward(UL, UR);
        ControllerFlag = 1;
    }
}


void Level_4(void){
    uint32_t raw17,raw14,raw16;
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Motor_Init();

    // user TimerA1 to sample the IR sensors at 2000 Hz
    // replace this line with a call to TimerA1_Init()
    TimerA1_Init(&IRsampling, 250);

    Motor_Stop();
    Mode = 0;
    UR = UL = PWMNOMINAL;
    ADCflag = ControllerFlag = 0;   // semaphores

    ADC0_InitSWTriggerCh17_14_16();   // initialize channels 17,12,16
    ADC_In17_14_16(&raw17,&raw14,&raw16);  // sample
    LPF_Init(raw17,64);     // P9.0/channel 17
    LPF_Init2(raw14,64);    // P4.1/channel 12
    LPF_Init3(raw16,64);    // P9.1/channel 16

    // user SysTick to run the controller at 100 Hz with a priority of 2
    // replace this line with a call to SysTick_Init()
    SysTick_Init(480000, 2);

    Pause3();

    EnableInterrupts();
    while(1){
        if(Bump_Read()){ // collision
            Mode = 0;
            Motor_Stop();
            Pause3();
        }
        if(ControllerFlag){ // 100 Hz, not real time
            ControllerFlag = 0;
        }
    }
}

int main() {
    Level_1();
//    Level_2();
//    Level_3();
//    Level_4();
}


