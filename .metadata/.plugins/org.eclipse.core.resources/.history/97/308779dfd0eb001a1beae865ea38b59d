/*
 * Classifier.c
 * Runs on MSP432
 *
 * Student Name: Holden Caraway
 * Date: 30 Aug 2020
 * DOCUMENTATION:
 *
 *  Conversion function for a GP2Y0A21YK0F IR sensor and classification function
 *  to take 3 distance readings and determine what the current state of the robot is.
 *
 *  Created on: Jul 24, 2020
 *  Author: Captain Steven Beyer
 *
 * This example accompanies the book
 * "Embedded Systems: Introduction to Robotics,
 * Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 * For more information about my classes, my research, and my books, see
 * http://users.ece.utexas.edu/~valvano/
 *
 * Simplified BSD License (FreeBSD License
 *
 * Copyright (c) 2019, Jonathan Valvano, All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the FreeBSD Project.
 */

#include <stdint.h>
#include "../inc/Classifier.h"

#define IRSlope 1195172
#define IROffset -1058
#define IRMax 2552
#define SIDEMIN 212      // smallest side distance to wall in mm
#define SIDEMAX 354      // largest side distance to wall in mm
#define CENTERMIN 150    // min distance to wall in the front
#define CENTEROPEN 600   // distance to wall between open/blocked
#define IRMIN 50         // min possible reading of IR sensor
#define IRMAX 800        // max possible reading of IR sensor



/* Convert
* Calculate the distance in mm given the 14-bit ADC value
* D = 195172/(n - 1058)
*
* The maximum measurement distance for the sensor is 800 mm,
* so if the ADC value is less than 2552 (IRMAX), your
* function should return 800.
*
* Input
*   int32_t n:  14-bit ADC data
* Output
*   int32_t     Distance in mm
*/
int32_t Convert(int32_t n){

    int32_t result = 0;     // replace this line
    int32_t D = 0;          // declaring 32 bit integer D, to store distance

    if(n < IRMax) {             //if ADC sample value is less than IRMax
        result = IRMAX;       //set result to MaxDist for return
    }
    else {                                  //else, calculate distance using given formula and variables
        D = IRSlope / (n + IROffset);       //Distance calculation
        result = D;                         //result set equal to distance
    }

    return result;                          //returns value of result
}



//#define SIDEMAX 354        // largest side distance to wall in mm
//#define SIDEMIN 212        // smallest side distance to wall in mm
//#define CENTEROPEN 600     // distance to wall between open/blocked
//#define CENTERMIN 150      // min distance to wall in the front
//#define IRMIN 50           // min possible reading of IR sensor
//#define IRMax 2552         // max possible reading of IR sensor

/* Classify
* Utilize three distance values from the left, center, and right
* distance sensors to determine and classify the situation into one
* of many scenarios (enumerated by scenario)
*
* Input
*   int32_t Left: distance measured by left sensor
*   int32_t Center: distance measured by center sensor
*   int32_t Right: distance measured by right sensor
*
* Output
*   scenario_t: value representing the scenario the robot
*       currently detects (e.g. RightTurn, LeftTurn, etc.)
*/
scenario_t Classify(int32_t Left, int32_t Center, int32_t Right){

    scenario_t result;

       if ((Left < IRMIN) || (Left > IRMAX) || (Center < IRMIN) || (Center > IRMAX) || (Right < IRMIN) || (Right > IRMAX)) {
           result = Error;
       }
       else if ((Left < SIDEMIN) && (Right < SIDEMIN) && (Center < CENTERMIN)) {
           result = AllBlocked;
       }
       else if ((Left < SIDEMIN) && (Center < CENTERMIN)) {
           result = LeftCenterTooClose;
       }
       else if ((Right < SIDEMIN) && (Center < CENTERMIN)) {
           result = RightCenterTooClose;
       }
       else if ((Right < SIDEMIN) && (Left < SIDEMIN)) {
           result = BothTooClose;
       }
       else if (Left < SIDEMIN) {
           result = LeftTooClose;
       }
       else if (Right < SIDEMIN) {
           result = RightTooClose;
       }
       else if (Center < CENTERMIN) {
           result = CenterTooClose;
       }
       else if ((Center < CENTEROPEN)) {
           if ((Left < SIDEMAX) && (Right >= SIDEMAX)) {
               result = RightTurn;
           }
           else if ((Right < SIDEMAX) && (Left >= SIDEMAX)) {
               result = LeftTurn;
           }
           else if ((Right >= SIDEMAX) && (Left >= SIDEMAX)) {
               result = TeeJoint;
           }
           else if ((Right < SIDEMAX) && (Left < SIDEMAX)) {
               result = Blocked;
           }
       }
       else if ((Center >= CENTEROPEN)) {
           if ((Right > SIDEMAX) && (Left < SIDEMAX)) {
               result = RightJoint;
           }
           else if ((Left > SIDEMAX) && (Right < SIDEMAX)) {
               result = LeftJoint;
           }
           else if ((Right < SIDEMAX) && (Left < SIDEMAX)) {
               result = Straight;
           }
           else if ((Right > SIDEMAX) && (Left > SIDEMAX)) {
               result = CrossRoad;
           }
       }
//       else if ((Left < SIDEMIN) && (Center < CENTERMIN)) {
//           result = LeftCenterTooClose;
//       }
//       else if ((Right < SIDEMIN) && (Center < CENTERMIN)) {
//           result = RightCenterTooClose;
//       }
//       else if (Left < SIDEMIN) {
//           result = LeftTooClose;
//       }
//       else if (Left < SIDEMIN) {
//           result = RightTooClose;
//       }
//       else if (Center < CENTERMIN) {
//           result = CenterTooClose;
//       }
    return result;
}

