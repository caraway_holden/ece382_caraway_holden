; Cryptography.asm
; Runs on any Cortex M
; Student name:Holden Caraway
; Date: 24 Aug 2020
; Documentation statement: C2C Caleb Kelly assisted by explaining the process
				;of moving addresses and values in and out of registers
				;as they were incremented, encrypted, decrypted. In this, he
				;specifically helped in the encryption function by pointing out that LDRB
				;grabs the next byte of a register by specififying #1 in the third paramter
				;and that the LR must be pushed or popped in each subroutine in order to
				;properly save it and use it to branch to the correct location in the code.
				;I took his advice and implemented it through trial and error into my encrypt/decrypt
				;functions, allowing my code to properly execute the encrypt/decrypt function.
				;C2C Max Peterson assisted by pointing out that I may be both passing but not using
				;a register when I found that my decrypt function would continue infinitely with repeats
				;of the terminating character's hex value. I used his suggestion and used a STRB
				;of R5, [R2], #1 which I hadn't used before since I was passing in R2 but not using
				;it to store a byte of R5 into the address of R2 which allowed my decrypt function to
				;properly iterate through and decrypt the messages.




; Cryptography function to encrypt and decrypt a message using a 1 byte key.
; Capt Steven Beyer
; July 23 2020
;
;Simplified BSD License (FreeBSD License)
;Copyright (c) 2020, Steven Beyer, All rights reserved.
;
;Redistribution and use in source and binary forms, with or without modification,
;are permitted provided that the following conditions are met:
;
;1. Redistributions of source code must retain the above copyright notice,
;   this list of conditions and the following disclaimer.
;2. Redistributions in binary form must reproduce the above copyright notice,
;   this list of conditions and the following disclaimer in the documentation
;   and/or other materials provided with the distribution.
;
;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
;AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
;USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;The views and conclusions contained in the software and documentation are
;those of the authors and should not be interpreted as representing official
;policies, either expressed or implied, of the FreeBSD Project.

       .thumb
       .data
       .align 2
encrypted_message .space 32 ;reserves 32 bits for encrypted message
decrypted_message .space 32 ;reserves 32 bits for decrypted message
	   .text
       .align 2
M1	   .string 	"This is message1!#"	;variable messages
M2     .string 	"This is message2!#"
M3     .string 	"This is message3!#"

Key1   .byte 0xab		;keys to encrypt and decrypt messages
Key2   .byte 0xcd
Key3   .byte 0xef

Secret .word 0x71070107 , 0x2B62707A		;secret message hex to decrypt
	   .word 0x2A366231 , 0x27206227
	   .word 0x21623631 , 0x3131232E
	   .word 0x00006163

SecKey .byte 0x42	;secret message key for decrypting secret message

       .global Encrypt			;function for encrypting messages
       .global Decrypt			;function for decrypting messages
       .global main				;main function which loads registers and calls subfunctions

main:  .asmfunc

	;load registers
   	   LDR R0, AddrM3             ;stores message address in R0
       LDR R1, AddrKey3	          ;stores key address in R1
	   LDR R2, EncryptAddr	       ;location to store encrypted message


	BL Encrypt		; goes to encryption function

    ; load registers
       LDR R0, EncryptAddr			;stores address of encrypted message
	   LDR R1, AddrKey3				;stores address of message key
	   LDR R2, DecryptAddr			;stores address of decrypted message

	BL Decrypt      ; goes to decryption function




infiniteLoop		; stall here and observe the registers

	 MOV R7, #5		;moves decimal number 5 into R7

	 CMP R7, R7		;compares R7 to R7

	 BEQ infiniteLoop		;breaks to loop if equal, which they always are

    .endasmfunc

; pointers to variables

AddrM1 .word M1
AddrM2 .word M2
AddrM3 .word M3

AddrKey1 .word Key1
AddrKey2 .word Key2
AddrKey3 .word Key3

EncryptAddr .word encrypted_message
DecryptAddr .word decrypted_message

SecretAddr .word Secret
SecKeyAddr .word SecKey

    .end




