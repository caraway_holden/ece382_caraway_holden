/*
 * Classifier.h
 * Runs on MSP432
 *
 * Student name: Holden Caraway
 * Date: 30 AUG 2020
 * DOCUMENTATION: Capt Beyer explained that a single decimal being returned in variable truth
 * from Solution.c was in fact a value (3) being returned which did not have an
 * affiliated enumerated type in my classifier.h. In solving this, he also explained that the
 * enumerated types weren't individual as much as they were being added together to equal
 * other enumerated types given the conditions were met, which was critical in my understanding
 * of how it worked. Troubleshooting the if statements, Capt Beyer pointed out that I had compared
 * some Left/Rights with SIDEMIN instead of SIDEMAX when determining which side to turn on, as well
 * as the side to turn into should be compared with a GTE instead of just a GT. With this pointed out, I
 * was able to go back and change the rest myself with Capt Beyer verifying. His help allowed me to solve
 * a mystery value and its cause as well as correct minor mistakes in my code to allow it to function correctly.
 *
 * Conversion function for a GP2Y0A21YK0F IR sensor and classification function
 *  to take 3 distance readings and determine what the current state of the robot is.
 *
 *  Created on: Jul 24, 2020
 *  Author: Captain Steven Beyer
 *
 */

enum scenario {                 //these descriptions gained from Module4 Figures 1 and 2, and inferred knowledge from them
    Error = 0,                  //distances lower than minimum distance allowed
    LeftTooClose = 1,           //left distance too close
    RightTooClose = 2,          //right distance too close
    BothTooClose = 3,           //right and left distance too close
    CenterTooClose = 4,         //center distance too close
    LeftCenterTooClose = 5,     //left and center distance too close
    RightCenterTooClose = 6,    //right and center distance too close
    AllBlocked = 7,             //all directions are too close
    Straight = 8,               //continue going straight, no turns possible
    LeftTurn = 9,               //turn left
    RightTurn = 10,             //turn right
    TeeJoint = 11,              //both left and right turns possible
    LeftJoint = 12,             //left turn and straight is possible
    RightJoint = 13,            //right turn and straight is possible
    CrossRoad = 14,             //left right and straight are possible
    Blocked = 15                //dead end encountered
};
typedef enum scenario scenario_t;

scenario_t Classify(int32_t Left, int32_t Center, int32_t Right);           //classifies the scenario encountered by the robot

int32_t Convert(int32_t n);                 //converts raw 14-bit ADC data to distances in mm
