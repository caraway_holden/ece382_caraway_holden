; Convert.asm
; Runs on any Cortex M

; Student name: Holden Caraway
; Date: 25 Aug 2020
; Documentation: Maj Birrer assisted by troubleshooting error messages and
; providing guidance on potential solutions. For example, on line 74, I was originally
; attempting to store the results of an ADD into a register by adding that register and
; a declared constant. Maj Birrer suggested that I may be attempting to do too much
; in that line and should separate it by storing the constant in another register and
; then adding the two registers together. I took his advice and loaded the value into R1
; and then adding R1 and R0 together. When faced with the same issue on another line, I
; was able to properly separate it and make the code work. It also taught me to always keep
; the code simple as possible and to put constants in registers where I could to avoid the
; same problems.

; Conversion function for a GP2Y0A21YK0F IR sensor
; Jonathan Valvano
; July 11, 2019

; This example accompanies the book
;   "Embedded Systems: Introduction to Robotics,
;   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
; For more information about my classes, my research, and my books, see
; http://users.ece.utexas.edu/~valvano/
;
;Simplified BSD License (FreeBSD License)
;Copyright (c) 2019, Jonathan Valvano, All rights reserved.
;
;Redistribution and use in source and binary forms, with or without modification,
;are permitted provided that the following conditions are met:
;
;1. Redistributions of source code must retain the above copyright notice,
;   this list of conditions and the following disclaimer.
;2. Redistributions in binary form must reproduce the above copyright notice,
;   this list of conditions and the following disclaimer in the documentation
;   and/or other materials provided with the distribution.
;
;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
;AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
;USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;The views and conclusions contained in the software and documentation are
;those of the authors and should not be interpreted as representing official
;policies, either expressed or implied, of the FreeBSD Project.



       .thumb
       .text
       .align 2
       .global Convert


;------------Convert------------
; Calculate the distance in mm given the 14-bit ADC value
; D = 1195172/(n � 1058)
; Input: R0 is n, ADC value, 14-bit unsigned number 2552 to 16383
; Output: R0 is distance in mm
; If the ADC input is less than 2552, return 800 mm
; Modifies: R1, R2, R3

Convert:   .asmfunc

		PUSH {LR}		    ; saves the return address from the function

		MOV R1, #2552		; moves the decimal value 2552 into R1

		CMP R0, R1			; compares R0 with R1

		BLT output			; breaks to output if the R0 was less than R1

		LDR R1, IROffset	; loads R1 with the value IROffset

		ADD R0, R0, R1	 	; stores the sum of the value at R0 and IROffset to get the bottom portion of the distance equation

		LDR R1, IRSlope		; loads R1 with the value of IRSlope

		SDIV R0, R1, R0    	; stores the result of IRSlope being divided by the value at R0

		POP {LR}			; return LR from the stack to be used to return

        BX LR				; breaks to location held in LR

output
		POP {LR}			; pops LR from the stack to be used to return
		MOV R0, #800		; moves decimal value 800 into R0
		BX LR				; breaks to location held in LR

      .endasmfunc
      .align 4

IRSlope      .word 1195172	; variables
IROffset     .word -1058
IRMax        .word 2552

    .end
